<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>org.taskmanager</groupId>
    <artifactId>TaskManagerVol2</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <modules>
        <module>task-manager-ui</module>
        <module>task-manager</module>
    </modules>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.1.4</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <!-- project settings -->
        <java.version>21</java.version>
        <spring-boot.version>3.1.4</spring-boot.version>
        <maven.compiler.source>21</maven.compiler.source>
        <maven.compiler.target>21</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <!-- tools & libs -->
        <commons-lang.version>3.5</commons-lang.version>
        <commons-collections-version>4.4</commons-collections-version>
        <commons.text.version>1.10.0</commons.text.version>
        <lombok.version>1.18.30</lombok.version>
        <mapstruct.version>1.5.3.Final</mapstruct.version>
        <mapstruct-lombok.version>0.2.0</mapstruct-lombok.version>
        <flyway-version>7.11.0</flyway-version>
        <swagger-version>2.9.2</swagger-version>
        <swagger-ui-version>1.5.5</swagger-ui-version>
        <cache-hazelcast-version>4.2.1</cache-hazelcast-version>
        <oracle.version>23.2.0.0</oracle.version>
        <postgres-version>42.2.27</postgres-version>
        <springdoc.version>2.0.4</springdoc.version>
        <h2.version>2.1.214</h2.version>
        <vavr.version>0.10.4</vavr.version>
        <slf4j.version>2.0.6</slf4j.version>
        <logback.version>1.4.6</logback.version>
        <flyway.version>9.16.0</flyway.version>
        <jsypt.version>3.0.5</jsypt.version>
        <hibernate.version>6.1.5.Final</hibernate.version>
        <hibernate-cache.version>5.6.1.Final</hibernate-cache.version>

        <!-- TESTS -->
        <testcontainer.version>1.17.6</testcontainer.version>
        <testcontainer.rabbitmq.version>1.17.6</testcontainer.rabbitmq.version>
        <oracle-xe.version>1.17.6</oracle-xe.version>
        <spock.version>2.4-M1-groovy-4.0</spock.version>

        <!-- VARIABLES -->
        <resource.delimiter>@</resource.delimiter>
    </properties>

    <dependencyManagement>
        <dependencies>
            <!--Spring Boot Autoconfigure-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-autoconfigure</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!--Spring Boot Web-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web</artifactId>
                <version>3.1.0</version>
            </dependency>

            <!-- Spring REST support -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-web-services</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Security -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-security</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Resource Server -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-oauth2-resource-server</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Mapping for rest api -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-actuator</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- hibernate/jpa support -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-data-jpa</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Bean Validation -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-validation</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Cache-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-cache</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Spring Tools - RESTART etc-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-devtools</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>

            <!-- Spring Boot - Docker compose-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-docker-compose</artifactId>
                <scope>runtime</scope>
                <optional>true</optional>
                <version>${spring-boot.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-common</artifactId>
                <version>${springdoc.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springdoc</groupId>
                <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
                <version>${springdoc.version}</version>
            </dependency>

            <!-- Lombok -->
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>${lombok.version}</version>
                <scope>provided</scope>
            </dependency>

            <!-- MapStruct -->
            <dependency>
                <groupId>org.mapstruct</groupId>
                <artifactId>mapstruct</artifactId>
                <version>${mapstruct.version}</version>
            </dependency>

            <!-- Lombok with Mapstruct integration -->
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok-mapstruct-binding</artifactId>
                <version>${mapstruct-lombok.version}</version>
            </dependency>

            <!--Hibernate Model Generator-->
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-jpamodelgen</artifactId>
                <version>${hibernate.version}</version>
            </dependency>

            <!-- Hibernate cache -->
            <dependency>
                <groupId>org.hibernate</groupId>
                <artifactId>hibernate-ehcache</artifactId>
                <version>${hibernate-cache.version}</version>
            </dependency>

            <!-- Default DB-->
            <dependency>
                <groupId>com.h2database</groupId>
                <artifactId>h2</artifactId>
                <version>${h2.version}</version>
                <scope>runtime</scope>
            </dependency>

            <!-- Flyway -->
            <dependency>
                <groupId>org.flywaydb</groupId>
                <artifactId>flyway-core</artifactId>
                <version>${flyway.version}</version>
            </dependency>

            <!-- Logs -->
            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>jul-to-slf4j</artifactId>
                <version>${slf4j.version}</version>
            </dependency>

            <dependency>
                <groupId>ch.qos.logback</groupId>
                <artifactId>logback-classic</artifactId>
                <version>${logback.version}</version>
            </dependency>

            <!-- Vavr -->
            <dependency>
                <groupId>io.vavr</groupId>
                <artifactId>vavr</artifactId>
                <version>${vavr.version}</version>
            </dependency>

            <!-- Apache Commons-->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-lang3</artifactId>
                <version>${commons-lang.version}</version>
            </dependency>

            <!-- Apache Commons Text-->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-text</artifactId>
                <version>${commons.text.version}</version>
            </dependency>

            <!-- Apache Commons IO-->
            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>2.13.0</version>
            </dependency>

            <!-- Apache Tika -->
            <dependency>
                <groupId>org.apache.tika</groupId>
                <artifactId>tika-core</artifactId>
                <version>0.7</version>
            </dependency>

            <!-- Apache Commons Collections-->
            <dependency>
                <groupId>org.apache.commons</groupId>
                <artifactId>commons-collections4</artifactId>
                <version>${commons-collections-version}</version>
            </dependency>

            <!-- TESTS -->
            <!-- Spring Boot tests -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-test</artifactId>
                <version>${spring-boot.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- Spock Core -->
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-core</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- Spock Spring integration -->
            <dependency>
                <groupId>org.spockframework</groupId>
                <artifactId>spock-spring</artifactId>
                <version>${spock.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- TestContainers with Spock integration -->
            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>spock</artifactId>
                <version>${testcontainer.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- TestContainers - oracle db-->
            <dependency>
                <groupId>org.testcontainers</groupId>
                <artifactId>oracle-xe</artifactId>
                <version>${oracle-xe.version}</version>
                <scope>test</scope>
            </dependency>

            <!-- ORACLE -->
            <dependency>
                <groupId>com.oracle.database.jdbc</groupId>
                <artifactId>ojdbc11</artifactId>
                <version>${oracle.version}</version>
            </dependency>

            <!-- PostgreSQL -->
            <dependency>
                <groupId>org.postgresql</groupId>
                <artifactId>postgresql</artifactId>
                <version>${postgres-version}</version>
            </dependency>

            <!--TO use Spring Data Projections with Spring Specifications -->
            <!--https://walczak.it/pl/blog/spring-data-jpa-projekcja-danych-zapytaniach-dynamicznych
            https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#projections-->
 <!--           <dependency>
                <groupId>com.cosium.spring.data</groupId>
                <artifactId>spring-data-jpa-entity-graph</artifactId>
                <version>2.0.1</version>
            </dependency>-->

<!--
            <dependency>
                <groupId>th.co.geniustree.springdata.jpa</groupId>
                <artifactId>specification-with-projections</artifactId>
                <version>2.0.1</version>
            </dependency>
-->

        </dependencies>
    </dependencyManagement>

</project>
package org.taskmanager.user.domain;

import org.springframework.beans.factory.annotation.Value;
import org.taskmanager.user.dto.UserProjectionDto;

import java.time.LocalDateTime;
import java.util.UUID;

interface UserProjection {

    long getId();

    UUID getUuid();

    String getUsername();

    @Value("#{target.getUpdateDate()}")                                                                                 //@Value("#{target.firstName + ' ' + target.lastName}")
    LocalDateTime getLastUpdateDate();

    default UserProjectionDto toDto() {
        return UserProjectionDto.builder()
                .withId(this.getId())
                .withUuid(this.getUuid())
                .withUsername(this.getUsername())
                .withLastUpdateDate(this.getLastUpdateDate())
                .build();
    }

}

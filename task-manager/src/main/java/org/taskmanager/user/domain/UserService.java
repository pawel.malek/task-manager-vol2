package org.taskmanager.user.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.taskmanager.user.dto.CreateUserDto;
import org.taskmanager.user.dto.UserDto;

import java.util.List;


@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserService {

    UserRepository userRepository;

    public UserDto create(CreateUserDto createUserDto) {
        User user = User.create(createUserDto);
        user = this.userRepository.save(user);
        return user.toDto();
    }

    public List<UserDto> findAll() {
        return this.userRepository.findAll()
                .stream()
                .map(User::toDto)
                .toList();
    }

}

package org.taskmanager.user.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.DynamicUpdate;
import org.taskmanager.shared.domain.BaseEntity;
import org.taskmanager.task.dto.UpdateTaskDto;
import org.taskmanager.user.dto.CreateUserDto;
import org.taskmanager.user.dto.UserDto;

import java.util.UUID;

@Getter
@Entity
@Table(name = "USERY")
@DynamicUpdate                                                                                                          // hibernate - feature to fix sql performance during update entity
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
class User extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id_gen")
    @SequenceGenerator(name = "user_id_gen", sequenceName = "user_id_seq", initialValue = 1, allocationSize = 1)
    Long id;

    @Column(name = "UUID", length = 64, unique = true, nullable = false, columnDefinition = "uuid")
    UUID uuid;

    @Column(name = "USER_NAME", unique = true, length = 64)
    String username;

    @Column(name = "PASSWORD", length = 128)
    String password;

    @Column(name = "EMAIL", length = 128)
    String email;

    static User create(CreateUserDto createUserDto) {
        return User.builder()
                .withUuid(UUID.randomUUID())
                .withUsername(createUserDto.getUsername())
                .withPassword(createUserDto.getPassword())
                .withEmail(createUserDto.getEmail())
                .build();
    }

    User update(UpdateTaskDto updateTaskDto) {
        return this;
    }

    UserDto toDto() {
        return UserDto.builder()
                .withId(this.id)
                .withUuid(this.uuid)
                .withUsername(this.username)
                .withPassword(this.password)
                .withEmail(this.email)
                .withCreateDate(this.createDate)
                .withUpdateDate(this.updateDate)
                .build();
    }

}

package org.taskmanager.user.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserProjectionDto {

    Long id;

    UUID uuid;

    String username;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    LocalDateTime lastUpdateDate;

    @Override
    public String toString() {
        return "UserDto{"  + '\'' +
                "id=" + id  + '\'' +
                ", uuid=" + uuid  + '\'' +
                ", username='" + username + '\'' +
                ", lastUpdateDate=" + lastUpdateDate  + '\'' +
                '}';
    }
}

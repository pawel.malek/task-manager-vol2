package org.taskmanager.user.infrastructure;

import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.taskmanager.shared.dto.PageableCriteria;
import org.taskmanager.task.domain.TaskService;
import org.taskmanager.task.dto.CreateTaskDto;
import org.taskmanager.task.dto.TaskDto;
import org.taskmanager.task.dto.TaskProjectionDto;
import org.taskmanager.task.dto.TaskSearchCriteriaDto;
import org.taskmanager.task.dto.UpdateTaskDto;
import org.taskmanager.user.domain.UserService;
import org.taskmanager.user.dto.CreateUserDto;
import org.taskmanager.user.dto.UserDto;

import java.util.List;

@Validated
@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api/user")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class UserController {

    UserService userService;

    @PostMapping(value = "")
    UserDto create(@Valid @RequestBody CreateUserDto createUserDto) {
        return this.userService.create(createUserDto);
    }

    @GetMapping(value = "")
    List<UserDto> findAll() {
        return this.userService.findAll();
    }
}

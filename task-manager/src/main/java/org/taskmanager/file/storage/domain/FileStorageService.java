package org.taskmanager.file.storage.domain;

import io.vavr.control.Try;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.taskmanager.file.storage.dto.CreateFileInfoDto;
import org.taskmanager.file.storage.dto.FileInfoDto;
import org.webjars.NotFoundException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;


/**
 * TODO learn more
 * <a href="https://www.callicoder.com/spring-boot-file-upload-download-rest-api-example/">learn more</a>
 */
@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FileStorageService {

    FileStorageConfiguration configuration;
    FileInfoRepository fileInfoRepository;

    public FileInfoDto uploadFile(UUID referenceUUID, MultipartFile multipartFile) {
        return Try.of(() -> this.storeFile(multipartFile, referenceUUID))
                .onFailure(throwable -> log.error("Store file failed"))
                .getOrElseThrow(() -> new RuntimeException("Store file failed"));
    }

    public List<FileInfoDto> uploadFiles(UUID referenceUUID, List<MultipartFile> multipartFiles) {
        return multipartFiles.stream()
                .map(multipartFile -> this.uploadFile(referenceUUID, multipartFile))
                .toList();
    }

    public ResponseEntity<Resource> downloadFile(UUID fileUUID) {
        FileInfoDto fileInfoDto = this.findFileInfoByUUIDs(List.of(fileUUID))
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException("File not found"));

        Path filePath = Path.of(fileInfoDto.getPath());
        Resource resource = Try.of(() -> new UrlResource(filePath.toAbsolutePath().toUri()))
                .getOrElseThrow(() -> new RuntimeException("Failed on read file path"));

        MediaType mediaType = MediaType.parseMediaType(
                StringUtils.defaultString(fileInfoDto.getMimeType(), "application/octet-stream"));

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""
                + StringUtils.defaultString(fileInfoDto.getName(), "no-name-file") + "\"");

        return ResponseEntity.ok()
                .contentType(mediaType)
                .headers(httpHeaders)
                .body(resource);
    }

    private FileInfoDto storeFile(MultipartFile file, UUID ownerUUID) {
        Path directoryPath = Path.of(this.configuration.getStoragePath(), ownerUUID.toString());
        createDirectory(directoryPath);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
        String timestamp = LocalDateTime.now().format(formatter);
        Path targetFilePath = directoryPath.resolve(timestamp);

        Try.of(() -> Files.copy(file.getInputStream(), targetFilePath, StandardCopyOption.REPLACE_EXISTING))
                .onFailure(throwable -> {
                    throw new RuntimeException(throwable.getMessage());
                });

        CreateFileInfoDto createFileInfo = CreateFileInfoDto.builder()
                .withName(file.getOriginalFilename())
                .withMimeType(file.getContentType())
                .withChecksum(this.createChecksum(file))
                .withPath(targetFilePath.toString())
                .withOwnerUUID(ownerUUID)
                .build();

        return this.fileInfoRepository.save(FileInfo.create(createFileInfo)).toDto();
    }

    public List<FileInfoDto> findByOwnerUUID(UUID ownerUUID) {
        return this.fileInfoRepository
                .findByOwnerUUID(ownerUUID)
                .stream()
                .map(FileInfo::toDto)
                .toList();
    }

    private List<FileInfoDto> findFileInfoByUUIDs(List<UUID> uuids) {
        return this.fileInfoRepository
                .findByIdIn(uuids)
                .stream()
                .map(FileInfo::toDto)
                .toList();
    }

    private String createChecksum(MultipartFile file) {
        try (InputStream in = file.getInputStream()) {
            return DigestUtils.md5DigestAsHex(in);
        } catch (IOException e) {
            return "";
        }
    }

    private void createDirectory(Path directoryPath) {
        File directory = directoryPath.toFile();

        if (!directory.exists()) {
            boolean created = directory.mkdir();
            if (created) {
                log.info("Directory created: " + directoryPath);
            } else {
                log.info("Failed to create directory: " + directoryPath);
            }
        } else {
            log.info("Directory already exists: " + directoryPath);
        }
    }

}

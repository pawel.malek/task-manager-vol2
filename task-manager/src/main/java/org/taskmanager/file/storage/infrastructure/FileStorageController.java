package org.taskmanager.file.storage.infrastructure;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.taskmanager.file.storage.domain.FileStorageService;
import org.taskmanager.file.storage.dto.FileInfoDto;
import org.taskmanager.task.dto.TaskProjectionDto;

import java.util.List;
import java.util.UUID;

@Validated
@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api/files")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class FileStorageController {

    FileStorageService fileStorageService;

    @GetMapping(value = "/owner/{owner-uuid}")
    List<FileInfoDto> getFilesInfoByOwnerUUID(@PathVariable("owner-uuid") UUID ownerUUID) {
        return this.fileStorageService.findByOwnerUUID(ownerUUID);
    }

    @PostMapping(value = "/owner/{owner-uuid}/upload-file")
    FileInfoDto uploadFile(@PathVariable("owner-uuid") UUID ownerUUID,
                           @RequestParam("file-descriptor") MultipartFile file) {
        return this.fileStorageService.uploadFile(ownerUUID, file);
    }

    @PostMapping(value = "/owner/{owner-uuid}/upload-files")
    List<FileInfoDto> uploadFiles(@PathVariable("owner-uuid") UUID referenceUUID,
                                  @RequestParam("files-descriptor") List<MultipartFile> files) {
        return this.fileStorageService.uploadFiles(referenceUUID, files);
    }

    @GetMapping(value = "/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable("fileUUID") UUID fileUUID) {
        return this.fileStorageService.downloadFile(fileUUID);
    }

}

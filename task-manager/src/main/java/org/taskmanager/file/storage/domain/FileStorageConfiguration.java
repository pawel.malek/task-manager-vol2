package org.taskmanager.file.storage.domain;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "app.config.file-storage")
public class FileStorageConfiguration {
    String storagePath;
}

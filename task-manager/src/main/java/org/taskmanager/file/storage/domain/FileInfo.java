package org.taskmanager.file.storage.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;
import org.taskmanager.file.storage.dto.CreateFileInfoDto;
import org.taskmanager.file.storage.dto.FileInfoDto;
import org.taskmanager.shared.domain.BaseEntity;

import java.util.UUID;

@Getter
@Entity
@DynamicUpdate
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "FILE_INFO")
@SuperBuilder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FileInfo extends BaseEntity {

    @Id
    @Column(name = "ID", updatable = false, nullable = false, columnDefinition = "uuid")
    @GeneratedValue(generator = "uuid-hibernate-generator")
    @GenericGenerator(name = "uuid-hibernate-generator", strategy = "org.hibernate.id.UUIDGenerator")
    UUID id;

    @Column(name = "OWNER_UUID", length = 16, unique = true, columnDefinition = "uuid")
    UUID ownerUUID;

    @Column(name = "NAME", unique = true, nullable = false, length = 32)
    String name;

    @Column(name = "MIME_TYPE", unique = true, nullable = false, length = 32)
    String mimeType;

    @Column(name = "PATH", nullable = false, length = 255)
    String path;

    @Column(name = "CHECKSUM", nullable = false, length = 64)
    String checksum;

    static FileInfo create(CreateFileInfoDto createFileInfoDto) {
        return FileInfo.builder()
                .withOwnerUUID(createFileInfoDto.getOwnerUUID())
                .withName(createFileInfoDto.getName())
                .withPath(createFileInfoDto.getPath())
                .withMimeType(createFileInfoDto.getMimeType())
                .withChecksum(createFileInfoDto.getChecksum())
                .build();
    }

    FileInfoDto toDto() {
        return FileInfoDto.builder()
                .withId(this.id)
                .withOwnerUUID(this.ownerUUID)
                .withName(this.name)
                .withPath(this.path)
                .withMimeType(this.mimeType)
                .withChecksum(this.checksum)
                .withCreateDate(this.createDate)
                .withUpdateDate(this.updateDate)
                .build();
    }

}

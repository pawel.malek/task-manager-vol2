package org.taskmanager.file.storage.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Set;
import java.util.UUID;

interface FileInfoRepository extends JpaRepository<FileInfo, UUID>, JpaSpecificationExecutor<FileInfo> {

    Set<FileInfo> findByOwnerUUID(UUID ownerUUID);
    Set<FileInfo> findByIdIn(List<UUID> uuids);
}

package org.taskmanager;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;

@SpringBootApplication
@EnableConfigurationProperties
public class Application {

    private final org.apache.logging.log4j.Logger log4j = LogManager.getLogger(Application.class);

    private final org.slf4j.Logger slf4j = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @EventListener(ApplicationStartedEvent.class)
    public void onStartUpWithApplicationStartedEvent() {
        log4j.log(Level.INFO, "ON STARTUP with onStartUpWithApplicationStartedEvent");
    }

    @Bean
    CommandLineRunner onStartUpWithCommandLineRunner() {
        return args -> {
            slf4j.info("ON STARTUP with onStartUpWithCommandLineRunner");
        };
    }

}

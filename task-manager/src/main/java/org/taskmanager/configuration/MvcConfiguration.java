package org.taskmanager.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.PathResourceResolver;

import java.io.IOException;

@Configuration
@EnableWebMvc
public class MvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry resourceHandlerRegistry) {
        resourceHandlerRegistry.addResourceHandler("/gui", "/gui/", "/gui/**")
                .addResourceLocations("classpath:/static/")
                .setCacheControl(CacheControl.noStore().mustRevalidate())
                .setCacheControl(CacheControl.noCache())
                .resourceChain(false)
                .addResolver(new ResourceResolver());
                //.addTransformer(new IndexTransformer(GUI_PREFIX));

    }

    private static class ResourceResolver extends PathResourceResolver {
        @Override
        protected Resource getResource(String resourcePath, Resource location) throws IOException {
            Resource requested = location.createRelative(resourcePath);
            return requested.exists() && requested.isReadable()
                    ? requested
                    : new ClassPathResource("/static/index.html");
        }
    }

    //alternative use
    /*    private ResourceResolver getResourceResolver() {
        return new PathResourceResolver() {
            @Override
            protected Resource getResource(String resourcePath, Resource location) throws IOException {
                Resource requested = location.createRelative(resourcePath);
                return requested.exists() && requested.isReadable() ? requested : new ClassPathResource(STATIC_CONTEXT + "index.html");
            }
        };
    }*/
}

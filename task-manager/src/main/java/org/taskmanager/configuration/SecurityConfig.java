package org.taskmanager.configuration;

import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.oauth2.core.DelegatingOAuth2TokenValidator;
import org.springframework.security.oauth2.core.OAuth2TokenValidator;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtDecoders;
import org.springframework.security.oauth2.jwt.JwtIssuerValidator;
import org.springframework.security.oauth2.jwt.JwtTimestampValidator;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import java.time.Duration;


@Configuration
@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig {

    //TODO https://spring.io/guides/gs/securing-web/
    OAuth2ResourceServerProperties oauthProperties;

    @Bean
    MvcRequestMatcher.Builder mvcRequestMatcherBuilder(HandlerMappingIntrospector introspector) {
        return new MvcRequestMatcher.Builder(introspector);
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http,
                                           MvcRequestMatcher.Builder mvcRequestMatcherBuilder) throws Exception {

        return http
                //.securityMatcher("/api/**")
                .authorizeHttpRequests(httpRequests ->
                        httpRequests
                                .requestMatchers(mvcRequestMatcherBuilder.pattern("/**")).permitAll()
                                .requestMatchers(mvcRequestMatcherBuilder.pattern("/gui/**")).permitAll()
                                .requestMatchers(mvcRequestMatcherBuilder.pattern("/api/user/**")).hasRole("USER")
                                .requestMatchers(mvcRequestMatcherBuilder.pattern("/api/admin/**")).hasRole("ADMIN")
                                .requestMatchers(mvcRequestMatcherBuilder.pattern("/api/no-secure/**")).permitAll()
                                .requestMatchers(AntPathRequestMatcher.antMatcher("/h2-console/**")).permitAll()
                                .anyRequest().authenticated()
                )
         /*       .csrf((csrf) -> csrf
                        .ignoringRequestMatchers(mvc.matchers("/to", "/be", "/ignored"))
                )*/
                .sessionManagement(httpSecuritySessionManagementConfigurer ->
                        httpSecuritySessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                )
                .oauth2ResourceServer(oauth2 ->
                        oauth2.jwt(jwtConfigurer ->
                                jwtConfigurer
                                        .decoder(jwtDecoder(oauthProperties))
                                        .jwtAuthenticationConverter(jwtAuthenticationConverter())
                        )
                )
                .build();
    }

    @Bean
    public JwtDecoder jwtDecoder(OAuth2ResourceServerProperties oauthProperties) {
        NimbusJwtDecoder jwtDecoder = JwtDecoders.fromIssuerLocation(oauthProperties.getJwt().getIssuerUri());
        jwtDecoder.setJwtValidator(jwtOAuth2TokenValidator());
        return jwtDecoder;
    }

    private OAuth2TokenValidator<Jwt> jwtOAuth2TokenValidator() {
        OAuth2TokenValidator<Jwt> validators = new DelegatingOAuth2TokenValidator<>(
                //new JwtClaimValidator<>("study-instance-uids", this::validateStudyInstanceUIDs)
                 new JwtTimestampValidator(Duration.ofSeconds(60)),
                 new JwtIssuerValidator(this.oauthProperties.getJwt().getIssuerUri())
                //new JwtClaimValidator<List<String>>(JwtClaimNames.AUD, aud -> aud.contains("messaging"))
       /*         new JwtClaimValidator<List<String>>("study-instance-uids", new Predicate<List<String>>() {
                    @Override
                    public boolean test(List<String> studyInstanceUIDs) {
                        log.info("TOKEN VALIDATE1: {}", strings);
                        return true;
                    }
                })*/
        );

        return validators;
    }

/*    private boolean validateStudyInstanceUIDs(List<String> studyInstanceUIDs) {
        if (Pattern.compile("^/studies/.+$").matcher(this.httpRequest.getRequestURI()).matches()) {
            return studyInstanceUIDs.stream()
                    .anyMatch(studyInstanceUID -> this.httpRequest.getRequestURI()
                            .contains(String.format("/studies/%s", studyInstanceUID))
                    );
        }
        return true;
    }*/

    private JwtAuthenticationConverter jwtAuthenticationConverter() {
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
       // grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");

        JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }

}

/*    @Bean
    public InMemoryUserDetailsManager userDetailsService() {
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("user")
                .password("password")
                .roles("USER")
                .build();

        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("admin")
                .password("password")
                .roles("ADMIN", "USER")
                .build();
        return new InMemoryUserDetailsManager(user, admin);
    }*/

/*@Bean
public UserDetailsService getUserDetailsService() {
    return new UserDetailsServiceImpl();
}

@Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

@Bean
public BCryptPasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
}

public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();

    daoAuthenticationProvider.setUserDetailsService(getUserDetailsService());
    daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());

    return daoAuthenticationProvider;
}*/
    /*
    * @Bean
public UserDetailsService userDetailsService(BCryptPasswordEncoder bCryptPasswordEncoder) {
    InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
    manager.createUser(User.withUsername("user")
      .password(bCryptPasswordEncoder.encode("userPass"))
      .roles("USER")
      .build());
    manager.createUser(User.withUsername("admin")
      .password(bCryptPasswordEncoder.encode("adminPass"))
      .roles("USER", "ADMIN")
      .build());
    return manager;
}
    * */

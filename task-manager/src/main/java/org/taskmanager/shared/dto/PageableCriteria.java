package org.taskmanager.shared.dto;

import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.springframework.data.domain.Sort;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PageableCriteria {

    @NotNull
    Integer page;

    @NotNull
    Integer size;

    @Builder.Default
    Sort.Direction sortDirection = Sort.Direction.ASC;

    @Builder.Default
    String sortBy = "id";

    @Override
    public String toString() {
        return "PageableCriteria{" +
                "page=" + page +
                ", size=" + size +
                ", sortDirection=" + sortDirection +
                ", sortBy=" + sortBy +
                '}';
    }
}

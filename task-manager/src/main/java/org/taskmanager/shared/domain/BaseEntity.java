package org.taskmanager.shared.domain;

import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@MappedSuperclass
@AllArgsConstructor
@SuperBuilder(setterPrefix = "with")
public class BaseEntity extends Audit implements Serializable {

}

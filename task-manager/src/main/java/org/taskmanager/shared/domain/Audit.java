package org.taskmanager.shared.domain;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@MappedSuperclass
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(setterPrefix = "with")
public class Audit implements Serializable {

    @Column(name = "CREATE_DATE")
    protected LocalDateTime createDate;

    @Column(name = "UPDATE_DATE")
    protected LocalDateTime updateDate;

    @PrePersist
    void prePersist() {
        this.createDate = LocalDateTime.now();
        this.updateDate = LocalDateTime.now();
    }

    @PreUpdate
    void preUpdate() {
        this.updateDate = LocalDateTime.now();
    }

}

package org.taskmanager.shared.utils;

import io.micrometer.common.util.StringUtils;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

@UtilityClass
public class SpecificationUtils {

    static public <T> Specification<T> equalsOrContains(Expression<String> entityPath, Object searchValue) {
        return (root, query, criteriaBuilder) -> {
            if (searchValue instanceof String stringValue) {
                if (StringUtils.isBlank(stringValue)) {
                    return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
                }
                return equalsOrLikePredicate(criteriaBuilder, entityPath, stringValue);
            } else if (Objects.isNull(searchValue)) {
                return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
            }
            return criteriaBuilder.equal(entityPath, searchValue);
        };
    }

    /**
     * The same as equalsOrContains - for tests only
     * */
    public static <T> Specification<T> equalsOrContainsV2(Expression<String> entityPath, Object searchValue) {
        return new Specification<T> () {
            @Override
            public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (searchValue instanceof String stringValue) {
                    if (StringUtils.isBlank(stringValue)) {
                        return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
                    }
                    return equalsOrLikePredicate(criteriaBuilder, entityPath, stringValue);
                }
                return criteriaBuilder.equal(entityPath, searchValue);
            }
        };
    }

    static public Predicate equalsOrLikePredicate(CriteriaBuilder criteriaBuilder, Expression<String> entityPath, String searchValue) {
        searchValue = searchValue.toLowerCase().trim();
        if (searchValue.startsWith("*") || searchValue.endsWith("*")) {
            return criteriaBuilder.like(criteriaBuilder.lower(entityPath), searchValue.replace("*", "%"));
        }
        return criteriaBuilder.equal(criteriaBuilder.lower(entityPath), searchValue);
    }


}

package org.taskmanager.task.dto;

public enum StatusDto {
    CREATED,
    TODO,
    SUSPENDED,
    IN_PROGRESS,
    FINISHED
}
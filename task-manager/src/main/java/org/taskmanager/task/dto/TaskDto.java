package org.taskmanager.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskDto {

    Long id;

    UUID workerUUID;

    UUID creatorUUID;

    String code;

    String title;

    String descriptionWithFormat;

    PriorityDto priority;

    StatusDto status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    LocalDateTime createDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    LocalDateTime updateDate;

    @Override
    public String toString() {
        String descriptionTrimmed = descriptionWithFormat.substring(0, Math.min(descriptionWithFormat.length(), 64));
        return "TaskDto{" +
                "id='" + id + '\'' +
                ", workerUUID='" + workerUUID + '\'' +
                ", creatorUUID='" + creatorUUID + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", descriptionWithFormat='" + descriptionTrimmed + '\'' +
                ", priority=" + priority +
                ", status=" + status +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                '}';
    }

}

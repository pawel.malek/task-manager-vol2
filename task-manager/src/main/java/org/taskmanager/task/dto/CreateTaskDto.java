package org.taskmanager.task.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateTaskDto {

    UUID workerUUID;

    @NotNull
    UUID creatorUUID;

    @NotBlank
    @Size(min = 3, max = 16)
    String code;

    @NotBlank
    @Size(min = 3, max = 64)
    String title;

    @Size(max = 1000)
    String descriptionWithFormat;

    @NotNull
    PriorityDto priority;

    @Override
    public String toString() {
        String descriptionTrimmed = descriptionWithFormat.substring(0, Math.min(descriptionWithFormat.length(), 64));
        return "CreateTaskDto{" +
                "code='" + code + '\'' +
                ", workerUUID='" + workerUUID + '\'' +
                ", creatorUUID='" + creatorUUID + '\'' +
                ", title='" + title + '\'' +
                ", descriptionWithFormat='" + descriptionTrimmed + '\'' +
                ", priority=" + priority +
                '}';
    }
}

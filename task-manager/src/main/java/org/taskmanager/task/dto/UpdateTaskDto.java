package org.taskmanager.task.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UpdateTaskDto {

    @NotNull
    Long id;

    @NotBlank
    @Size(min = 3, max = 16)
    String code;

    @NotBlank
    @Size(min = 3, max = 64)
    String title;

    @Size(max = 1000)
    String descriptionWithFormat;

    @NotNull
    PriorityDto priority;

    @NotNull
    StatusDto status;

    @Override
    public String toString() {
        String descriptionTrimmed = descriptionWithFormat.substring(0, Math.min(descriptionWithFormat.length(), 64));
        return "UpdateTaskDto{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", descriptionWithFormat='" + descriptionTrimmed + '\'' +
                ", priority=" + priority +
                ", status=" + status +
                '}';
    }
}

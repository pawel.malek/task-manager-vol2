package org.taskmanager.task.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskProjectionDto {

    Long id;

    UUID workerUUID;

    String code;

    String title;

    StatusDto status;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    LocalDateTime lastUpdateDate;

    @Override
    public String toString() {
        return "TaskDto{" +
                "id='" + id + '\'' +
                ", workerUUID='" + workerUUID + '\'' +
                ", code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", status=" + status +
                ", lastUpdateDate=" + lastUpdateDate +
                '}';
    }

}

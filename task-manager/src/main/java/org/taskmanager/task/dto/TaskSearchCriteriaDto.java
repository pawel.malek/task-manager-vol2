package org.taskmanager.task.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskSearchCriteriaDto {

    String code;

    String title;

    String description;

    PriorityDto priority;

    StatusDto status;

    @Override
    public String toString() {
        return "SearchCriteriaDto{" +
                "code='" + code + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority + '\'' +
                ", status=" + status + '\'' +
                '}';
    }

}

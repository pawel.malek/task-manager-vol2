package org.taskmanager.task.dto;

public enum PriorityDto {
    CRITICAL,
    IMPORTANT,
    NORMAL,
    MINOR
}
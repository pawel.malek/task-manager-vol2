package org.taskmanager.task.infrastructure;

import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.taskmanager.file.storage.dto.FileInfoDto;
import org.taskmanager.shared.dto.PageableCriteria;
import org.taskmanager.task.domain.TaskService;
import org.taskmanager.task.dto.CreateTaskDto;
import org.taskmanager.task.dto.TaskDto;
import org.taskmanager.task.dto.TaskProjectionDto;
import org.taskmanager.task.dto.TaskSearchCriteriaDto;
import org.taskmanager.task.dto.UpdateTaskDto;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Validated
@CrossOrigin
@RestController
@AllArgsConstructor
@RequestMapping("/api/task")
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class TaskController {

    TaskService taskService;

    @PostMapping(value = "")
    TaskDto create(@Valid @RequestBody CreateTaskDto createTaskDto) {
        return this.taskService.create(createTaskDto);
    }

    @PutMapping(value = "")
    TaskDto update(@Valid @RequestBody UpdateTaskDto updateTaskDto) {
        return this.taskService.update(updateTaskDto);
    }

    @GetMapping(value = "")
    Page<TaskDto> search(@Valid @ModelAttribute TaskSearchCriteriaDto searchCriteriaDto,
                         @Valid @ModelAttribute PageableCriteria pageableCriteria) {
        return this.taskService.search(searchCriteriaDto, pageableCriteria);
    }

    @GetMapping(value = "/view")
    List<TaskProjectionDto> getProjections() {
        return this.taskService.getProjections();
    }
}

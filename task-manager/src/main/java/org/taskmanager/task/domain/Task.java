package org.taskmanager.task.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Column;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GenerationType;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.EnumType;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.EnumUtils;
import org.hibernate.annotations.DynamicUpdate;
import org.taskmanager.shared.domain.BaseEntity;
import org.taskmanager.task.dto.CreateTaskDto;
import org.taskmanager.task.dto.PriorityDto;
import org.taskmanager.task.dto.StatusDto;
import org.taskmanager.task.dto.TaskDto;
import org.taskmanager.task.dto.UpdateTaskDto;

import java.util.UUID;

@Getter
@Entity
@DynamicUpdate                                                                                                          // hibernate - feature to fix sql performance during update entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "TASKS")
@SuperBuilder(setterPrefix = "with")
@FieldDefaults(level = AccessLevel.PRIVATE)
class Task extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "task_id_gen")
    @SequenceGenerator(name = "task_id_gen", sequenceName = "task_id_seq", initialValue = 1, allocationSize = 1)
    Long id;

    @Column(name = "UUID", length = 16, nullable = false, unique = true, columnDefinition = "uuid")
    UUID uuid;

    @Column(name = "WORKER_UUID", length = 16, unique = true, columnDefinition = "uuid")
    UUID workerUUID;

    @Column(name = "CREATOR_UUID", length = 16, unique = true, nullable = false, columnDefinition = "uuid")
    UUID creatorUUID;

    @Column(name = "CODE", unique = true, nullable = false, length = 16)
    String code;

    @Column(name = "TITLE", nullable = false, length = 64)
    String title;

    @Column(name = "DESCRIPTION_WITH_FORMAT", columnDefinition="TEXT")
    String descriptionWithFormat;

    @Enumerated(EnumType.STRING)
    @Column(name = "PRIORITY", nullable = false, length = 16)
    Priority priority;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS", nullable = false, length = 16)
    Status status;

    static Task create(CreateTaskDto createTaskDto) {
        return Task.builder()
                .withUuid(UUID.randomUUID())
                .withCreatorUUID(createTaskDto.getCreatorUUID())
                .withWorkerUUID(createTaskDto.getWorkerUUID())
                .withCode(createTaskDto.getCode())
                .withTitle(createTaskDto.getTitle())
                .withDescriptionWithFormat(createTaskDto.getDescriptionWithFormat())
                .withPriority(EnumUtils.getEnum(Priority.class, createTaskDto.getPriority().name()))
                .withStatus(Status.CREATED)
                .build();
    }

    Task update(UpdateTaskDto updateTaskDto) {
        this.code = updateTaskDto.getCode();
        this.title = updateTaskDto.getTitle();
        this.descriptionWithFormat = updateTaskDto.getDescriptionWithFormat();
        this.priority = EnumUtils.getEnum(Priority.class, updateTaskDto.getPriority().name());
        this.status = EnumUtils.getEnum(Status.class, updateTaskDto.getStatus().name());
        return this;
    }

    TaskDto toDto() {
        return TaskDto.builder()
                .withId(this.id)
                .withCreatorUUID(this.creatorUUID)
                .withWorkerUUID(this.workerUUID)
                .withCode(this.code)
                .withTitle(this.title)
                .withDescriptionWithFormat(this.descriptionWithFormat)
                .withPriority(EnumUtils.getEnum(PriorityDto.class, this.priority.name()))
                .withStatus(EnumUtils.getEnum(StatusDto.class, this.status.name()))
                .withCreateDate(this.createDate)
                .withUpdateDate(this.updateDate)
                .build();
    }

}

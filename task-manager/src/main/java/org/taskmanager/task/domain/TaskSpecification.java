package org.taskmanager.task.domain;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.From;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.taskmanager.shared.utils.SpecificationUtils;
import org.taskmanager.task.dto.TaskSearchCriteriaDto;

/**
 * TODO learn more
 * <a href="https://reflectoring.io/spring-data-specifications/">learn more</a>
 * */
@NoArgsConstructor
@AllArgsConstructor
@Builder(setterPrefix = "with")
class TaskSpecification implements Specification<Task> {

    TaskSearchCriteriaDto searchCriteria;

    @Override
    public Predicate toPredicate(Root<Task> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        return getSearchSpecifications(root)
                .toPredicate(root, query, criteriaBuilder);
    }

    <T> Specification<Task> getSearchSpecifications(From<T, Task> root) {
        return Specification.allOf(
                SpecificationUtils.equalsOrContains(root.get(Task_.CODE), searchCriteria.getCode()),
                SpecificationUtils.equalsOrContains(root.get(Task_.TITLE), searchCriteria.getTitle()),
                SpecificationUtils.equalsOrContains(root.get(Task_.DESCRIPTION_WITH_FORMAT), searchCriteria.getDescription()), // TODO - change to full text search
                SpecificationUtils.equalsOrContains(root.get(Task_.STATUS), searchCriteria.getStatus()),
                SpecificationUtils.equalsOrContains(root.get(Task_.PRIORITY), searchCriteria.getPriority())
        );
    }
}

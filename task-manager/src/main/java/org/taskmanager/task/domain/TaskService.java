package org.taskmanager.task.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.taskmanager.shared.dto.PageableCriteria;
import org.taskmanager.task.dto.CreateTaskDto;
import org.taskmanager.task.dto.TaskProjectionDto;
import org.taskmanager.task.dto.TaskSearchCriteriaDto;
import org.taskmanager.task.dto.TaskDto;
import org.taskmanager.task.dto.UpdateTaskDto;
import org.webjars.NotFoundException;

import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskService {

    TaskRepository taskRepository;

    public TaskDto create(CreateTaskDto createTaskDto) {
        log.debug("Create new task: {}", createTaskDto);
        Task newTask = Task.create(createTaskDto);
        newTask = this.taskRepository.save(newTask);
        return newTask.toDto();
    }

    public TaskDto update(UpdateTaskDto updateTaskDto) {
        log.debug("Update task: {}", updateTaskDto);
        Task existingTask = taskRepository.findById(updateTaskDto.getId())
                .orElseThrow(() -> new NotFoundException(String.format("Task with id: %s not found", updateTaskDto.getId())));
        existingTask = existingTask.update(updateTaskDto);
        return existingTask.toDto();
    }

    public Page<TaskDto> search(TaskSearchCriteriaDto searchCriteria, PageableCriteria pageableCriteria) {
        log.debug("Search tasks by search criteria: {}", searchCriteria);
        Sort sortOptions = Sort.by(pageableCriteria.getSortDirection(), pageableCriteria.getSortBy());
        PageRequest pageRequest = PageRequest.of(pageableCriteria.getPage(), pageableCriteria.getSize(), sortOptions);
        return this.taskRepository.findAll(new TaskSpecification(searchCriteria), pageRequest)
                .map(Task::toDto);
    }

    public TaskDto getById(Long id) {
        return this.taskRepository.findById(id)
                .map(Task::toDto)
                .orElseThrow(() -> new NotFoundException(String.format("Task with id: %s not found", id)));
    }

    public List<TaskProjectionDto> getProjections() {
        return this.taskRepository.findProjectedBy()
                .stream()
                .map(TaskProjection::toDto)
                .toList();
    }

}

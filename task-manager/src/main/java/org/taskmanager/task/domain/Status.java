package org.taskmanager.task.domain;

import org.taskmanager.task.dto.StatusDto;

enum Status {
    CREATED,
    TODO,
    SUSPENDED,
    IN_PROGRESS,
    FINISHED;

    public StatusDto toDto() {
        return StatusDto.valueOf(this.name());
    }
}
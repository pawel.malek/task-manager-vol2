package org.taskmanager.task.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Set;
import java.util.UUID;

interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {

    Set<TaskProjection> findByStatus(Status status);

    Set<TaskProjection> findProjectedBy();

    Set<Task> findProjectedByWorkerUUID(UUID workerUUID);

    Set<Task> findByWorkerUUID(UUID workerUUID);
}

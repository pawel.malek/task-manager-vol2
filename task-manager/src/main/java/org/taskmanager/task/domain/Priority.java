package org.taskmanager.task.domain;

enum Priority {
    CRITICAL,
    IMPORTANT,
    NORMAL,
    MINOR
}
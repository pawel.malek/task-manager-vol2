package org.taskmanager.task.domain;

import org.springframework.beans.factory.annotation.Value;
import org.taskmanager.task.dto.TaskProjectionDto;

import java.time.LocalDateTime;
import java.util.UUID;

interface TaskProjection {

    long getId();

    UUID getWorkerUUID();

    String getCode();

    String getTitle();

    @Value("#{target.getStatus()}")                                                                                 //@Value("#{target.firstName + ' ' + target.lastName}")
    Status getStatus();

    @Value("#{target.getUpdateDate()}")                                                                                 //@Value("#{target.firstName + ' ' + target.lastName}")
    LocalDateTime getLastUpdateDate();

    default TaskProjectionDto toDto() {
        return TaskProjectionDto.builder()
                .withId(this.getId())
                .withWorkerUUID(this.getWorkerUUID())
                .withCode(this.getCode())
                .withTitle(this.getTitle())
                .withStatus(this.getStatus().toDto())
                .withLastUpdateDate(this.getLastUpdateDate())
                .build();
    }

}
